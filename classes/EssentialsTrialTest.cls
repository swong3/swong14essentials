@isTest
public with sharing class EssentialsTrialTest {

    @isTest
    static public void testSayHello() {
        
        String expected = 'Hello World';
        EssentialsTrial obj = new EssentialsTrial();

        System.assertEquals(expected, obj.sayHello(), 'Hello');

    }

}